package com.example.pietrogirardi.butterknifeexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.example.pietrogirardi.butterknifeexample.adapters.ProductAdapter;
import com.example.pietrogirardi.butterknifeexample.models.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListActivityTest extends AppCompatActivity {

    @Bind(R.id.lvProducts) protected ListView lvProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_test);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Random randoPrice = new Random();

        List<Product> list = new ArrayList<Product>();
        list.add(new Product("Product 1", "Produtos de limpeza", 5 + randoPrice.nextInt(100)));
        list.add(new Product("Product 2", "Produtos de limpeza", 5 + randoPrice.nextInt(100)));
        list.add(new Product("Product 3", "Produtos de limpeza", 5 + randoPrice.nextInt(100)));
        list.add(new Product("Product 4", "Produtos de limpeza", 5 + randoPrice.nextInt(100)));
        list.add(new Product("Product 5", "Produtos de limpeza", 5 + randoPrice.nextInt(100)));
        list.add(new Product("Product 6", "Produtos de limpeza", 5 + randoPrice.nextInt(100)));
        list.add(new Product("Product 7", "Produtos de limpeza", 5 + randoPrice.nextInt(100)));
        list.add(new Product("Product 8", "Produtos de limpeza", 5 + randoPrice.nextInt(100)));
        list.add(new Product("Product 9", "Produtos de limpeza", 5 + randoPrice.nextInt(100)));
        list.add(new Product("Product 10", "Produtos de limpeza", 5 + randoPrice.nextInt(100)));

        ButterKnife.bind(ListActivityTest.this);
        lvProducts.setAdapter(new ProductAdapter(ListActivityTest.this, list));

    }

}

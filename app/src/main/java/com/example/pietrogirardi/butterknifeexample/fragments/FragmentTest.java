package com.example.pietrogirardi.butterknifeexample.fragments;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pietrogirardi.butterknifeexample.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FragmentTest extends Fragment {

    @Bind(R.id.btToastMessage) protected Button btToastMessage;
    @Bind({R.id.etName, R.id.etEmail}) protected List<EditText> listEt;

    public FragmentTest() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.fragment_fragment_test, container, false);

            ButterKnife.bind(FragmentTest.this, view);

            for (int i = 0, tam = listEt.size(); i < tam; i++) {
                Log.i("LOG", "Hint: " + ((TextInputLayout) listEt.get(i).getParent()).getHint().toString());
            }

        // Inflate the layout for this fragment
        return view;
    }

    @OnClick(R.id.btToastMessage)
    public void callToast(){
        Toast.makeText(getActivity(), "Toast printado", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
